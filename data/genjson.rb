#!/usr/bin/env ruby
#
# Generate JSON files with lists of things. Our database is
# slow so we'll create a few files.
require 'csv'
require 'json'

SYMBOLS = [ :payees, :agencies, :accounts ]
FILES = Dir.glob('/src/data/nebraska-expenditures-*.csv')
DATA = { :payees => [], :agencies => [], :accounts => [] }
OUTPUTS = {
  :payees => '/src/web/js/data/payees.json',
  :agencies => '/src/web/js/data/agencies.json',
  :accounts => '/src/web/js/data/accounts.json'
}

FILES.each do |file|
  CSV.foreach(file, :encoding => 'iso-8859-1:utf-8', :headers => true) do |row|
    DATA[:payees].push(row[2]) if not row[2].nil?
    DATA[:agencies].push(row[4]) if not row[4].nil?
    DATA[:accounts].push(row[6]) if not row[6].nil?
  end
end

SYMBOLS.each do |sym|
  File.open(OUTPUTS[sym.to_sym], 'w') do |f|
    f.write(DATA[sym.to_sym].uniq.sort.to_json)
  end
end

all_data = []
SYMBOLS.each do |sym|
  DATA[sym.to_sym].each do |value|
    all_data.push(value)
  end
end
File.open('/src/web/js/data/all.json', 'w') do |f|
  f.write(all_data.uniq.sort.to_json)
end
