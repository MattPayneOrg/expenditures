# Hack Lincoln 2014

## About

This is the repository for our [Hack Lincoln](http://hacklincoln.org) 2014 project.

## Overview

This repository contains the following:

1. A [Vagrant](http://www.vagrantup.com) configuration to set up a VM with the following: [CentOS](https://www.centos.org) 7, [Python](http://python.org) 2.7, Python 3, [PostgreSQL](http://postgresql.org) 9.3, [PostGIS](http://postgis.net) 2.1, various Python libraries, [RVM](http://rvm.io), [Ruby](http://ruby-lang.org) 2.1.2, and the latest versions of [node.js](http://nodejs.org), [npm](http://npmjs.org), [Bower](http://bower.io), [LESS](http://lesscss.org), [SASS](http://sass-lang.com), [CoffeeScript](http://coffeescript.org), and [js2coffee](http://js2coffee.org).

## Use

To set up a development environment:

1. Install [Vagrant](http://www.vagrantup.com) and your provider of choice ([VirtualBox](https://www.virtualbox.org) is free)
2. Clone this repository (e.g., `git clone https://rossnelson@bitbucket.org/rossnelson/expenditures.git ~/hl2014`)
3. Go to the `vagrant` directory (e.g., `cd ~/hl2014/vagrant`)
4. Start the VM with `vagrant up`
5. Once it has installed and configured the VM, connect to it with `vagrant ssh`