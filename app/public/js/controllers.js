// app.js - angular app for Expenditures

	var API_PREFIX = 'http://localhost:9292';

	// autocomplete for the search field
		$("#searchfield").autocomplete({
	    source: function (request, response) {
	        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
	        $.ajax({
	            url: "js/data/agencies.json",
	            dataType: "json",
	            success: function (data) {
	                response($.map(data, function(v,i){
	                    var text = v;
	                    if ( text && ( !request.term || matcher.test(text) ) ) {
	                        return {
	                                label: v
	                               };
	                    }
	                }));
	            }
	        });
	    }
	});

var expendituresApp = angular.module('expendituresApp', ['angularUtils.directives.dirPagination']);

expendituresApp.controller('ExpendituresMainCtrl', function ($rootScope, $scope, $http) {
	console.log($rootScope.currentSearch);
	$http.get(API_PREFIX + '/api/v1/expenditures?offset=0&limit=100')
       .then(function(data){
	      
          $scope.items = data.data;   
          createChartsForAgency();     
        });
        
	function createChartsForAgency() {
		var chartData = [];
		console.log($scope.items);
		for(i=0; i<$scope.items.length; i++) {
			chartData.push({
				value: parseFloat($scope.items[i].amount),
				label: $scope.items[i].agency_name,
				color: getRandomColor(),
				highlight: getRandomColor()
		});
		};
		console.log(chartData);
		var ctx = $("#myChart").get(0).getContext("2d");
		var myPieChart = new Chart(ctx).Pie(chartData);
		$('.jumbotron p').text(myPieChart.generateLegend());
    }
});

expendituresApp.controller('ExpendituresYearCtrl', function ($scope, $http) {
	
	
	$http.get(API_PREFIX + '/api/v1/date/2009/07/06?offset=0&limit=100')
       .then(function(data){
          $scope.items = data.data;               
        });
});

expendituresApp.controller('ExpendituresAgencyCtrl', function ($scope, $http) {
	
	$http.get(API_PREFIX + '/api/v1/expenditures?offset=0&limit=100')
       .then(function(data){
          $scope.items = data.data;              
        });
});

expendituresApp.controller('searchController', function ($rootScope, $scope, $http) {
	
	
	if(!$rootScope.currentSearch === undefined && $('#searchField').submit()) {
	$http.get(API_PREFIX + '/api/v1/agency/' + $rootScope.currentSearch + '?offset=40&limit=20')
       .then(function(data){
          $scope.items = data.data; 
        });
     }
        
    $scope.$watch("search", function(newVal, oldVal) { $rootScope.currentSearch = newVal; });
});
	
	function getRandomColor() {
	    var letters = '0123456789ABCDEF'.split('');
	    var color = '#';
	    for (var i = 0; i < 6; i++ ) {
	        color += letters[Math.floor(Math.random() * 16)];
	    }
	    return color;
	}