require 'sinatra/base'
require 'sinatra/config_file'
require 'active_support/all'
require 'date'
require 'pg'
require 'json'
require 'yaml'

# /api/v1/*
class ApiV1 < Sinatra::Base
  @@base_query = 'select payment_date, payee, amount, agency_name, account, program_description from expenditures'
  @@start_year = 2009
  @@end_year = 2014

  get '/expenditures' do
    format_response(query(@@base_query, params), request.accept)
  end

  get '/date/:year/:month/:day' do
    p_date = params[:year] + '-' + params[:month] + '-' + params[:day]
    q = @@base_query + " where payment_date='#{p_date}'"
    format_response(query(q, params), request.accept)
  end

  get '/accounts/:account_name' do
    q = @@base_query + " where account='#{params[:account_name]}' "
    format_response(query(q, params), request.accept)
  end

  get '/agencies/:name' do
    q = @@base_query + " where agency_name='#{params[:name]}' "
    format_response(query(q, params), request.accept)
  end

  get '/agencies/:name/summary' do
    q = @@base_query + " where agency_name='#{params[:name]}' "
    data = query(q, params)
    overview = {}

    (@@start_year .. @@end_year).each do |year|
      yearly_overview = {}

      (1..12).each do |month|
        summary_data = query("select sum(amount) from expenditures where #{month_where(month, year)}", nil)

        total_amount = summary_data.getvalue(0, 0)
        total_amount = '0.0' if total_amount.nil?
        yearly_overview[month.to_s] = total_amount
      end

      overview[year.to_s] = yearly_overview
    end

    response = {
        'overview' => overview,
        'data' => data
    }
    format_response(response, request.accept)
  end

  get '/agencies/:agency/accounts/:account' do
    query_where = " where agency_name='#{params[:agency]}' and account='#{params[:account]}' "
    q = @@base_query + query_where
    data = query(q, params)

    overview = {}
    overview['payees'] = []

    payee_count = query("select count(distinct payee) from expenditures" + query_where, params)
    payees = query("select distinct payee from expenditures" + query_where, params)
    payees.each do |p_data|
      payee = p_data['payee'].to_s.gsub("'", "\\'")
      payee_sum_data = query("select sum(amount) from expenditures" + query_where + " and payee='#{payee}'", params)

      payee_sum = payee_sum_data.getvalue(0, 0).to_s
      payee_sum = '0.0' if payee_sum.nil?

      overview['payees'].push({'name' => payee, 'sum' => payee_sum})
    end

    overview['payee_count'] = payee_count.getvalue(0, 0)

    response = {
        'overview' => overview,
        'data' => data
    }
    format_response(response, request.accept)
  end

  get '/agencies/:name/:year/:month' do
    month = Integer(params[:month])
    year = Integer(params[:year])

    query_where = " where agency_name='#{params[:name]}' and #{month_where(month, year)} "
    q = @@base_query + query_where
    data = query(q, params)
    overview = {}

    accounts = query("select distinct account from expenditures" + query_where, params)
    accounts.each do |a_data|
      account = a_data['account']

      summary_data = query("select sum(amount) from expenditures where agency_name='#{params[:name]}' and account='#{account}' and #{month_where(month, year)}", params)
      total_amount = summary_data.getvalue(0, 0)
      total_amount = '0.0' if total_amount.nil?
      overview[account] = total_amount
    end

    response = {
        'overview' => overview,
        'data' => data
    }
    format_response(response, request.accept)
  end

  get '/agencies/:name/:year/:month/:account' do
    month = Integer(params[:month])
    year = Integer(params[:year])

    q = @@base_query + " where agency_name='#{params[:name]}' and #{month_where(month, year)} and account='#{params[:account]}' "
    format_response(query(q, params), request.accept)
  end




  def format_response(data, accept)
    accept.each do |type|
      return data.to_xml  if type.downcase.eql? 'text/xml'
      return data.to_json if type.downcase.eql? 'application/json'
      return data.to_yaml if type.downcase.eql? 'text/x-yaml'
      return data.to_csv  if type.downcase.eql? 'text/csv'
      return data.to_json
    end
  end

  def dbconn
    config = YAML.load_file('config/config.yml')
    PG::Connection.new(config['production']['db_host'],
                       5432,
                       nil,
                       nil,
                       config['production']['db_name'],
                       config['production']['db_user'],
                       config['production']['db_pass'])
  end

  def query(q, params)
    # Grab the offset and limit if they were specified
    offset = Integer(params[:offset]) rescue nil
    limit = Integer(params[:limit]) rescue nil

    # Build the query
    query = q
    query = query + " offset #{offset.to_s}" if offset
    query = query + " limit #{limit.to_s}" if limit

    # Query the database
    dbconn.exec(query)
  end

  def month_where(month, year)
    _m = "#{year.to_s}-#{month.to_s}"
    start_date = _m + '-01'
    end_date = _m + '-' + Date.civil(year, month, -1).day.to_s
    "payment_date >= '#{start_date}' and payment_date <= '#{end_date}'"
  end
end
