require 'bundler'
require 'sinatra/base'
require 'sinatra/config_file'
require 'json'
require './api/init'
Bundler.require

class Expenditures < Sinatra::Base
  register Sinatra::ConfigFile
  
  configure do
    set :app_file, __FILE__
    set :run, true
    set :server, %w[thin mongrel webrick]
    set :cookie_options, { path: '/' }
    set :sessions, true
    set :root, File.join(File.dirname(__FILE__), '../')
  end
  
  # Load the user's config file
  set :environments, %w{process development production}
  config_file 'config/config.yml'

  error do
    halt 500, 'Server error'
  end
  not_found do
    halt 404, 'File not found'
  end
  
  get '/' do
    @ua = settings.ua
    if @ua
      @ua_key = settings.ua_key
      @ua_dom = settings.ua_dom
    end
    
    erb :index
  end
end
