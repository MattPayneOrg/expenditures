drop database if exists expenditures;
create database expenditures;

drop user if exists expenditures;
create user expenditures with superuser password 'QMv9APLKJPCOB4fabC3w57jSONDmbZ1OrMNeh';
alter database expenditures owner to expenditures;
