drop table if exists expenditures;
create table expenditures (
  id serial,
  row_id integer,
  payment_date date,
  payee varchar(255),
  amount double precision,
  agency_name varchar(255),
  account varchar(255),
  program_description varchar(500)
);

set datestyle = 'ISO, MDY';
set client_encoding to 'latin1';

copy expenditures (row_id, payment_date, payee, amount, agency_name, account, program_description)
  from '/src/data/nebraska-expenditures-2009.csv' with delimiter ',' csv header;
copy expenditures (row_id, payment_date, payee, amount, agency_name, account, program_description)
  from '/src/data/nebraska-expenditures-2010.csv' with delimiter ',' csv header;
copy expenditures (row_id, payment_date, payee, amount, agency_name, account, program_description)
  from '/src/data/nebraska-expenditures-2011.csv' with delimiter ',' csv header;
copy expenditures (row_id, payment_date, payee, amount, agency_name, account, program_description)
  from '/src/data/nebraska-expenditures-2012.csv' with delimiter ',' csv header;
copy expenditures (row_id, payment_date, payee, amount, agency_name, account, program_description)
  from '/src/data/nebraska-expenditures-2013.csv' with delimiter ',' csv header;
copy expenditures (row_id, payment_date, payee, amount, agency_name, account, program_description)
  from '/src/data/nebraska-expenditures-2014.csv' with delimiter ',' csv header;

-- psql -h localhost -U expenditures -W -d expenditures  #pw=83QMv9APLKJPCOB4fabC3w57jSONDmbZ1OrMNeh