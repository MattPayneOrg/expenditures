#!/usr/bin/bash

cd /tmp
wget http://labix.org/download/python-dateutil/python-dateutil-1.5.tar.gz
tar zxf python-dateutil-1.5.tar.gz
cd python-dateutil-1.5
sudo python setup.py install