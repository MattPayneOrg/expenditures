#!/usr/bin/bash

echo "if [ ! -f ~/.installedrvm ]; then" >> ~vagrant/.profile
echo "  echo 'Installing RVM and Ruby 2.1.2'" >> ~vagrant/.profile
echo "  curl -L https://get.rvm.io | bash -s stable --auto-dotfiles >/dev/null && source ~/.rvm/scripts/rvm && rvm install --default 2.1.2 >/dev/null && gem install bundler >/dev/null" >> ~vagrant/.profile
echo "  echo 'Installing PostgreSQL gem (pg)'" >> ~vagrant/.profile
echo "  gem install pg" >> ~vagrant/.profile
echo "  echo 'pg gem installed'" >> ~vagrant/.profile
echo "  touch ~/.installedrvm" >> ~vagrant/.profile
echo "fi" >> ~vagrant/.profile
echo "source ~/.rvm/scripts/rvm" >> ~vagrant/.profile
echo "source ~/.profile" >> ~vagrant/.bash_profile
chown vagrant:vagrant ~vagrant/.profile