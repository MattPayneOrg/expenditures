#!/usr/bin/bash

# Update everything first, if applicable
sudo yum -y update

# Install some system utilities
sudo yum -y install vim-enhanced openssl-devel gcc gcc-c++ binutils nano htop

# Install source control stuff
sudo yum -y install git git-svn libgit2-devel subversion subversion-devel

# Install Python 2.7 and easy_install
sudo yum -y install python python-devel python-setuptools

# Install pip
sudo sudo easy_install pip

# Install a better date/time parser for python
/vagrant/shell/dateutil.sh