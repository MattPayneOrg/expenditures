#!/usr/bin/bash
# Install PostgreSQL and start it up, also set it to start at boot
sudo yum -y install http://yum.postgresql.org/9.3/redhat/rhel-7-x86_64/pgdg-centos93-9.3-1.noarch.rpm
sudo yum -y install postgresql93-server postgresql93-contrib postgresql93-devel postgresql-devel
sudo /usr/pgsql-9.3/bin/postgresql93-setup initdb
sudo systemctl enable postgresql-9.3.service
sudo systemctl start postgresql-9.3.service

# Install EPEL (needed for PostGIS dependencies)
sudo yum -y install /vagrant/software/epel-release-7-0.2.noarch.rpm

# Set PostgreSQL to listen on TCP/IP
echo "listen_addresses = '*'" >> /var/lib/pgsql/9.3/data/postgresql.conf

# Allow the fi user to log in
sudo cp /vagrant/data/pg_hba.conf /var/lib/pgsql/9.3/data/pg_hba.conf
sudo chown postgres:postgres /var/lib/pgsql/9.3/data/pg_hba.conf
sudo chmod 600 /var/lib/pgsql/9.3/data/pg_hba.conf

# Restart the database
sudo service postgresql-9.3 restart

# You can now connect to the database with the following:
#    psql -h localhost -U fi -W -d foodinspections #pw=83k4ursbL79a7